﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {
	public Camera mCamera;
	public GameObject mPrefab;
	public GameObject mPrefab2;
	public GUIText mText;
	
	int score=0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 dir=mCamera.ScreenToWorldPoint(Input.mousePosition);
		Vector3 pos=this.transform.position;
		Vector3 diff=0.01f*(dir-pos);
		diff.x=0;
		this.transform.Translate(diff,Space.World);	
	}
	
	void  OnCollisionEnter( Collision cl) {
		Debug.Log ("Collision");
		if (cl.collider.name.Equals("UnitCyl(Clone)")){
			if (this.transform.localScale.x<5.0f){
				this.transform.localScale+=new Vector3(0.1f,0.0f,0.1f);
			}
			Vector3 pos=new Vector3(0.0f,Random.Range(-10,10),Random.Range(-10,10));
			Instantiate(mPrefab,pos,Quaternion.Euler(0, 0, 90));
			score++;
		}else{
			if (this.transform.localScale.x>0.5f){
				this.transform.localScale-=new Vector3(0.1f,0.0f,0.1f);
			}
			Vector3 pos=new Vector3(0.0f,Random.Range(-10,10),Random.Range(-10,10));
			Instantiate(mPrefab2,pos,Quaternion.Euler(0, 0, 90));
			score--;
		}
		
		Destroy(cl.collider.gameObject);
		mText.text=""+score;
	}
}
